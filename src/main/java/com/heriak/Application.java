package com.heriak;

import com.heriak.view.View;

/**
 * Class for running game.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public class Application {
    /**
     * Runs game through method show().
     *
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        new View().show();
    }
}
