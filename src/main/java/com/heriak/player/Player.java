package com.heriak.player;

import com.heriak.model.Ship;
import com.heriak.model.field.Battlefield;
import com.heriak.model.field.Field;

import java.util.List;

public abstract class Player implements Shootable {
    /**
     * Field with allied ships.
     */
    private Field battleField;

    private List<StringBuilder> mainMap;
    private List<StringBuilder> additionalMap;
    /**
     * String which represents status of made shot or played game.
     */
    private String message;
    /**
     * Name of the player.
     */
    private String name;
    /**
     * GameOver trigger.
     */
    private boolean gameOver;

    public Player(String name) {
        battleField = new Battlefield();
        mainMap = battleField.generateMap(true);
        additionalMap = battleField.generateMap(false);
        message = "";
        this.name = name;
    }

    /**
     * Returns main map with player's ships.
     *
     * @return symbol map with ships
     */
    public List<StringBuilder> getMainMap() {
        return mainMap;
    }

    /**
     * Returns additional map without ships (map for marking shots).
     *
     * @return symbol map without ships
     */
    public List<StringBuilder> getAdditionalMap() {
        return additionalMap;
    }

    /**
     * Returns message which represents status of made shot or played game.
     *
     * @return string message
     */
    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Returns player's name.
     *
     * @return player's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the end of the game.
     */
    public void setGameOver() {
        gameOver = true;
    }

    /**
     * Checks if game is over or not.
     *
     * @return true if every allied ship is sunk and false if not
     */
    public boolean isGameOver() {
        for (Ship ship : battleField.getShips()) {
            if (!ship.isSunk()) {
                return false || gameOver;
            }
        }
        message = name + " LOSE!";
        return true;
    }

    /**
     * Makes shot at given coordinates simultaneously changing enemy
     * and allied maps.
     * Common part of implementation makeShot method.
     *
     * @param x     coordinate X of shot
     * @param y     coordinate Y of shot
     * @param enemy enemy player
     * @return status of made shot
     */
    public String shootAtCoords(int x, int y, Player enemy) {
        for (Ship ship : enemy.battleField.getShips()) {
            if (ship.isSunk()) {
                continue;
            }
            if (ship.hasCoordinates(x, y)) {
                ship.takeShot(x, y);
                enemy.getMainMap().get(y).setCharAt(x, 'X');
                this.getAdditionalMap().get(y).setCharAt(x, 'X');
                if (ship.isSunk()) {
                    return "SUNK!";
                } else {
                    return "HIT!";
                }
            }
        }
        enemy.getMainMap().get(y).setCharAt(x, '*');
        this.getAdditionalMap().get(y).setCharAt(x, '*');
        return "MISSED!";
    }
}
