package com.heriak.player;

import com.heriak.exceptions.IllegalCoordinatesException;

/**
 * Interface which provides methods for game logic represented through players.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public interface Shootable {

    /**
     * Makes shot at the enemy player's ships.
     * Has a little different implementation in subclasses Human and Bot.
     * So the common part of this implementation are storing here.
     *
     * @param target coordinates of the shot
     * @param enemy enemy player
     */
    void shoot(String target, Player enemy) throws IllegalCoordinatesException;
}
