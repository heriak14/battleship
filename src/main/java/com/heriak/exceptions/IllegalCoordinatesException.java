package com.heriak.exceptions;

public class IllegalCoordinatesException extends Throwable {

    public IllegalCoordinatesException(String message) {
        super(message);
    }
}
