package com.heriak.view;

import com.heriak.model.Property;

/**
 * Enum with colors to print in console.
 */
public enum ConsoleColor {
    /**
     * Color of ship.
     */
    SHIP_COLOR(Property.INSTANCE.getProperty("ships.color")),
    /**
     * Color of successful made shot.
     */
    PRECISION_SHOT_COLOR(Property.INSTANCE.getProperty("shot.precision.color")),
    /**
     * Color of missed shot.
     */
    MISSED_SHOT_COLOR(Property.INSTANCE.getProperty("shot.missed.color")),
    /**
     * Color of background.
     */
    BACKGROUND_COLOR(Property.INSTANCE.getProperty("background.color")),
    /**
     * Color of waves in the sea.
     */
    WAVES_COLOR(Property.INSTANCE.getProperty("waves.color")),
    /**
     * Colors to reset other colors.
     */
    RESET(Property.INSTANCE.getProperty("reset.color"));

    private String colorCode;

    ConsoleColor(String colorCode) {
        this.colorCode = colorCode;
    }

    @Override
    public String toString() {
        return this.colorCode;
    }
}
