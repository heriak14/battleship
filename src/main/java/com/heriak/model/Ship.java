package com.heriak.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class which represents ship, stores some information about this ship
 * and contains a few checkers.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public class Ship {

    /**
     * Variable for storing the number of deck of the ship.
     */
    private int numOfDeck;
    /**
     * List of coordinates of each deck.
     */
    private List<List<Integer>> coordinates;
    /**
     * Represents position of the ship in battlefield.
     */
    private Orientation orientation;
    /**
     * Random numbers generator.
     */
    private static final Random RANDOM = new Random();

    /**
     * Constructor which initializes fields
     * and determines an orientation of ship.
     *
     * @param numOfDeck number of deck of the ship.
     */
    public Ship(final int numOfDeck) {
        this.numOfDeck = numOfDeck;
        coordinates = new ArrayList<>(numOfDeck);
        orientation = Orientation.values()[RANDOM.nextInt(
                Orientation.values().length)];
    }

    /**
     * Returns list of coordinates of each deck.
     *
     * @return list of coordinates.
     */
    public List<List<Integer>> getCoordinates() {
        return coordinates;
    }

    /**
     * Returns an orientation of the ship in battlefield.
     *
     * @return orientation of the ship.
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * Returns number of deck of the ship.
     *
     * @return number of deck.
     */
    public int getNumOfDeck() {
        return numOfDeck;
    }

    /**
     * Checks if one of the decks of ship has given coordinates.
     *
     * @param x coordinate X
     * @param y coordinate Y
     * @return true if ship has these coordinates and false if not
     */
    public boolean hasCoordinates(final int x, final int y) {
        List<Integer> xy = new ArrayList<>(2);
        xy.add(x);
        xy.add(y);
        return coordinates.contains(xy);
    }

    /**
     * Checks if ship is sunk, that is all its decks are destroyed.
     *
     * @return true if ship is sunk and false if not
     */
    public boolean isSunk() {
        return (numOfDeck == 0);
    }

    /**
     * Simulates a hit in one of the deck of ship.
     *
     * @param x coordinate X of hit
     * @param y coordinate Y of hit
     */
    public void takeShot(final int x, final int y) {
        List<Integer> coordinate = new ArrayList<>();
        coordinate.add(x);
        coordinate.add(y);
        coordinates.remove(coordinate);
        numOfDeck--;
    }
}
