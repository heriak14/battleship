package com.heriak.consts;

import com.heriak.model.Property;

public class FieldConst {

    /**
     * Size of square battlefield (NxN).
     */
    public static final int FIELD_SIZE = Integer.parseInt(Property.INSTANCE.getProperty("field.size"));
    /**
     * Number of ships on the battlefield.
     */
    public static final int NUM_OF_SHIPS = Integer.parseInt(Property.INSTANCE.getProperty("ships.num"));

    private FieldConst() {
    }
}
