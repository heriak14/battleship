package com.heriak.model.field;

import com.heriak.consts.FieldConst;
import com.heriak.model.Orientation;
import com.heriak.model.Ship;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Implementation of interface Field.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public class Battlefield implements Field {

    /**
     * Pseudo-random numbers generator.
     */
    private static final Random RANDOM = new Random();
    /**
     * Number of field coordinates (x and y).
     */
    private static final int NUM_OF_COORDINATES = 2;
    /**
     * Difference between coordinates to be considered intersectional.
     */
    private static final int DELTA = 1;
    /**
     * List of ships on the battlefield.
     */
    private List<Ship> ships;

    /**
     * Constructor which initializes fields
     * and calls method for generating ships.
     */
    public Battlefield() {
        ships = new ArrayList<>(FieldConst.NUM_OF_SHIPS);
        generateShips();
    }

    /**
     * Returns list of generated ships.
     *
     * @return list of ships
     */
    public List<Ship> getShips() {
        return ships;
    }

    /**
     * Generates list of ships each of which has coordinates
     * which don't intersects according to the rules.
     */
    @Override
    public void generateShips() {
        Ship ship;
        for (int i = 0; i < FieldConst.NUM_OF_SHIPS; i++) {
            if (i < 1) {
                ship = new Ship(4);
            } else if (i < 3) {
                ship = new Ship(3);
            } else if (i < 6) {
                ship = new Ship(2);
            } else {
                ship = new Ship(1);
            }
            setShipLocation(ship, ships);
            ships.add(ship);
        }
    }

    /**
     * Locate ship according to the list of already located ships.
     *
     * @param ship  ship for locating
     * @param ships list of already located ships
     */
    private void setShipLocation(final Ship ship, final List<Ship> ships) {
        do {
            int startX;
            int startY;
            ship.getCoordinates().clear();
            if (ship.getOrientation() == Orientation.HORIZONTAL) {
                startX = RANDOM.nextInt(FieldConst.FIELD_SIZE - ship.getNumOfDeck());
                startY = RANDOM.nextInt(FieldConst.FIELD_SIZE);
                setHorizontalCoordinates(startX, startY, ship);
            } else if (ship.getOrientation() == Orientation.VERTICAL) {
                startX = RANDOM.nextInt(FieldConst.FIELD_SIZE);
                startY = RANDOM.nextInt(FieldConst.FIELD_SIZE - ship.getNumOfDeck());
                setVerticalCoordinates(startX, startY, ship);
            }
        } while (this.intersect(ship, ships));
    }

    /**
     * Sets coordinates for horizontal-orientated ship.
     *
     * @param startX start coordinate X of ship
     * @param startY start coordinate Y for ship
     * @param ship   ship to set coordinates
     */
    private void setHorizontalCoordinates(final int startX, final int startY,
                                          final Ship ship) {
        for (int i = 0; i < ship.getNumOfDeck(); i++) {
            List<Integer> xy = new ArrayList<>(NUM_OF_COORDINATES);
            xy.add(startX + i);
            xy.add(startY);
            ship.getCoordinates().add(xy);
        }
    }

    /**
     * Sets coordinates for vertical-orientated ship.
     *
     * @param startX start coordinate X of ship
     * @param startY start coordinate Y for ship
     * @param ship   ship to set coordinates
     */
    private void setVerticalCoordinates(final int startX, final int startY,
                                        final Ship ship) {
        for (int i = 0; i < ship.getNumOfDeck(); i++) {
            List<Integer> xy = new ArrayList<>(NUM_OF_COORDINATES);
            xy.add(startX);
            xy.add(startY + i);
            ship.getCoordinates().add(xy);
        }
    }

    /**
     * Method for checking whether ship intersects with list of another ships
     * or not according to the game rules.
     *
     * @param ship  ship to check
     * @param ships list of generated ships
     * @return true if intersects and false if not
     */
    private boolean intersect(final Ship ship, final List<Ship> ships) {
        for (Ship s : ships) {
            for (List<Integer> point1 : ship.getCoordinates()) {
                for (List<Integer> point2 : s.getCoordinates()) {
                    if (hasCommonPoints(point1, point2)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if 2 points have coordinates that are equals or different by 1.
     *
     * @param point1 first point
     * @param point2 second point
     * @return true if points are the same or near
     */
    private boolean hasCommonPoints(final List<Integer> point1,
                                    final List<Integer> point2) {
        if ((point1.get(0) >= point2.get(0) - DELTA
                && point1.get(0) <= point2.get(0) + DELTA)
                && (point1.get(1) >= point2.get(1) - DELTA
                && point1.get(1) <= point2.get(1) + DELTA)) {
            return true;
        }
        return false;
    }

    /**
     * Generates symbol map of battlefield.
     *
     * @param withShips whether add ships on the map or not
     */
    @Override
    public List<StringBuilder> generateMap(final boolean withShips) {
        List<StringBuilder> map = new ArrayList<>(FieldConst.FIELD_SIZE);
        for (int i = 0; i < FieldConst.FIELD_SIZE; i++) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < FieldConst.FIELD_SIZE; j++) {
                if (withShips && this.haveCoordinates(j, i)) {
                    line.append("#");
                } else {
                    line.append("~");
                }
            }
            map.add(line);
        }
        return map;
    }

    /**
     * Checks if any of the ships on battlefield has following coordinates.
     *
     * @param x coordinate X
     * @param y coordinate Y
     * @return true if has coordinates and false if not
     */
    @Override
    public boolean haveCoordinates(int x, int y) {
        for (Ship ship : ships) {
            if (ship.hasCoordinates(x, y)) {
                return true;
            }
        }
        return false;
    }

}
