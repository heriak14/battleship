package com.heriak.model.field;

import com.heriak.model.Property;
import com.heriak.model.Ship;

import java.util.List;

/**
 * Interface which contains methods for creating battlefield
 * and generating its map.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public interface Field {

    /**
     * Generates list of ships each of which has coordinates
     * which don't intersects according to the rules.
     */
    void generateShips();

    /**
     * Returns list of generated ships.
     *
     * @return list of ships
     */
    List<Ship> getShips();

    /**
     * Generates symbol map of battlefield.
     *
     * @param withShips whether add ships on the map or not
     */
    List<StringBuilder> generateMap(boolean withShips);

    /**
     * Checks if any of the ships on battlefield has following coordinates.
     *
     * @param x coordinate X
     * @param y coordinate Y
     * @return true if has coordinates and false if not
     */
    boolean haveCoordinates(int x, int y);
}
