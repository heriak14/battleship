package com.heriak.player;

import com.heriak.consts.FieldConst;

import java.util.Random;

/**
 * Class which extends class Player and provides automatic random
 * shoots making. That is it gives an opportunity for player to play a game
 * with computer.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public class Bot extends Player {

    /**
     * Pseudo-random numbers generator.
     */
    private static final Random RAND = new Random();

    /**
     * Constructor which initializes fields and calls method for generating map.
     */
    public Bot() {
        super("Bot");
    }

    /**
     * Makes shot at the given coordinates of enemy player's ships.
     *
     * @param target coordinates of the shot
     * @param enemy  enemy player
     */
    @Override
    public void shoot(String target, final Player enemy) {
        int x;
        int y;
        do {
            x = RAND.nextInt(FieldConst.FIELD_SIZE);
            y = RAND.nextInt(FieldConst.FIELD_SIZE);
        } while (enemy.getMainMap().get(y).charAt(x) == 'X');
        this.setMessage(shootAtCoords(x, y, enemy));
    }
}
