package com.heriak.player;

import com.heriak.consts.FieldConst;
import com.heriak.exceptions.IllegalCoordinatesException;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class which extends class Player and provides manual shot making.
 * That is it gives an opportunity for player to play a game
 * with another player.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public class Human extends Player {

    /**
     * Pattern for checking validation of input coordinates.
     */
    private static final String PATTERN = "[a-j]([1-9]|10)";
    /**
     * Relation of letters coordinates and their numerical analogs.
     */
    private Map<Character, Integer> letterCoordinates;

    /**
     * Constructor which initializes fields and calls methods
     * for generating maps.
     *
     * @param name player's name
     */
    public Human(final String name) {
        super(name);
        letterCoordinates = new LinkedHashMap<>(FieldConst.FIELD_SIZE);
        String letters = "abcdefghij";
        for (int i = 0; i < letters.length(); i++) {
            letterCoordinates.put(letters.charAt(i), i);
        }
    }

    /**
     * Makes shot at the given coordinates of enemy player's ships.
     *
     * @param target coordinates of the shot
     * @param enemy  enemy player
     */
    @Override
    public void shoot(String target, final Player enemy) throws IllegalCoordinatesException {
        if (!target.matches(PATTERN)) {
            throw new IllegalCoordinatesException("Wrong coordinates!");
        }
        int x = letterCoordinates.get(target.charAt(0));
        int y = Integer.parseInt(target.substring(1)) - 1;
        String message = super.shootAtCoords(x, y, enemy);
        this.setMessage(message);
    }
}
