package com.heriak.view;

import com.heriak.consts.FieldConst;
import com.heriak.exceptions.IllegalCoordinatesException;
import com.heriak.player.Player;
import com.heriak.model.Property;
import com.heriak.player.Bot;
import com.heriak.player.Human;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class providing all visualization and process of the game.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public class View {
    /**
     * Logo of the game.
     */
    private static final String LOGO = " ______     ______     ______   ______ "
            + "  __         ______     ______     __  __     __     ______  \n"
            + "/\\  == \\   /\\  __ \\   /\\__  _\\ /\\__  _\\ /\\ \\       /\\"
            + "  ___\\   /\\  ___\\   /\\ \\_\\ \\   /\\ \\   /\\  == \\ \n"
            + "\\ \\  __<   \\ \\  __ \\  \\/_/\\ \\/ \\/_/\\ \\/ \\ \\ \\____"
            + "  \\ \\  __\\   \\ \\___  \\  \\ \\  __ \\  \\ \\ \\  \\ \\  _-/"
            + " \n \\ \\_____\\  \\ \\_\\ \\_\\    \\ \\_\\    \\ \\_\\  \\ \\"
            + "_____\\  \\ \\_____\\  \\/\\_____\\  \\ \\_\\ \\_\\  \\ \\_\\  "
            + "\\ \\_\\   \n  \\/_____/   \\/_/\\/_/     \\/_/     \\/_/   \\/"
            + "_____/   \\/_____/   \\/_____/   \\/_/\\/_/   \\/_/   \\/_/   \n"
            + "                                                               "
            + "                                     \n";
    /**
     * Scanner variable for reading input.
     */
    private static final Scanner SCANNER = new Scanner(System.in, "UTF-8");
    /**
     * Logger variable.
     */
    private static final Logger LOG = LogManager.getLogger();
    /**
     * Delay between players' turns.
     */
    private static final int TURN_DELAY;
    /**
     * Number of players in the game.
     */
    private static final int NUM_OF_PLAYERS;
    /**
     * Number of menu items.
     */
    private static final int NUM_OF_MENUS;
    /**
     * Player 1.
     */
    private Player player1;
    /**
     * Player 2.
     */
    private Player player2;
    /**
     * Menu for choosing game mode.
     */
    private Map<String, String> gameMenu;
    /**
     * Relation between entered menu keys and game-mode methods.
     */
    private Map<String, Runnable> menuMethods;
    /**
     * Game against bot indicator.
     */
    private boolean vsBot;

    static {
        TURN_DELAY = Integer.parseInt(Property.INSTANCE.getProperty("turn.delay"));
        NUM_OF_PLAYERS = Integer.parseInt(Property.INSTANCE.getProperty("players.num"));
        NUM_OF_MENUS = Integer.parseInt(Property.INSTANCE.getProperty("menus.num"));
    }

    /**
     * Constructor for creating and filling menus.
     */
    public View() {
        gameMenu = new LinkedHashMap<>(NUM_OF_MENUS);
        gameMenu.put("1", " PLAY VS COMPUTER");
        gameMenu.put("2", " PLAY VS ANOTHER PLAYER");

        menuMethods = new LinkedHashMap<>(NUM_OF_MENUS);
        menuMethods.put("1", this::playVSBot);
        menuMethods.put("2", this::playVSHuman);
    }

    /**
     * Turns on game mode to play against bot.
     */
    private void playVSBot() {
        LOG.trace("Enter player_1 name: ");
        player1 = new Human(SCANNER.nextLine());
        player2 = new Bot();
        vsBot = true;
    }

    /**
     * Turns on game mode to play against another player.
     * Initializes both players with read names.
     */
    private void playVSHuman() {
        LOG.trace("Enter player_1 name: ");
        player1 = new Human(SCANNER.nextLine());
        LOG.trace("Enter player_2 name: ");
        player2 = new Human(SCANNER.nextLine());
    }

    /**
     * Prints menu, takes user input and runs mode
     * according to chosen menu item.
     *
     * @throws IOException          exception thrown by cls() method
     * @throws InterruptedException exception thrown by cls() method
     */
    private void showMenu() throws IOException, InterruptedException {
        String mode;
        do {
            cls();
            LOG.trace(LOGO);
            for (String key : gameMenu.keySet()) {
                LOG.trace(key + gameMenu.get(key) + "\n");
            }
            LOG.trace("Choose game mode: ");
            mode = SCANNER.nextLine();
        } while (!mode.matches("[1-2]"));
        menuMethods.get(mode).run();
    }

    /**
     * Prints main and additional map of given player.
     *
     * @param player player to print maps
     */
    private void printMaps(final Player player) {
        LOG.trace(LOGO);
        for (int i = 0; i < NUM_OF_PLAYERS; i++) {
            LOG.trace("    a b c d e f g h i j ");
        }
        LOG.trace("\n");
        for (int i = 0; i < FieldConst.FIELD_SIZE; i++) {
            LOG.trace(String.format("%2d| ", (i + 1)));
            printLine(player.getMainMap().get(i));
            LOG.trace(String.format("%2d| ", (i + 1)));
            printLine(player.getAdditionalMap().get(i));
            LOG.trace("\n");
        }
    }

    /**
     * Prints every character from StringBuilder line with the specific color.
     *
     * @param line line to print
     */
    private void printLine(final StringBuilder line) {
        if (line == null) {
            return;
        }
        for (int i = 0; i < line.length(); i++) {
            char sym = line.charAt(i);
            String bgrColor = ConsoleColor.BACKGROUND_COLOR.toString();
            switch (sym) {
                case '~':
                    LOG.trace(bgrColor + ConsoleColor.WAVES_COLOR + sym + " " + ConsoleColor.RESET);
                    break;
                case 'X':
                    LOG.trace(bgrColor + ConsoleColor.PRECISION_SHOT_COLOR + sym + " " + ConsoleColor.RESET);
                    break;
                case '#':
                    LOG.trace(bgrColor + ConsoleColor.SHIP_COLOR + sym + " " + ConsoleColor.RESET);
                    break;
                case '*':
                    LOG.trace(bgrColor + ConsoleColor.MISSED_SHOT_COLOR + sym + " " + ConsoleColor.RESET);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Clears the console.
     *
     * @throws InterruptedException throws if something wrong with
     *                              process interruption.
     * @throws IOException          throws if something wrong with Input/Output APIs.
     */
    private void cls() throws InterruptedException, IOException {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
    }

    /**
     * Calls Player shoot method and prints result of the shot.
     *
     * @param shooter player which is making a shot.
     * @param target  player against which shot is making.
     * @param input   String of coordinates
     * @return true if shot was successful and false if not
     * @throws InterruptedException is thrown because of method sleep
     */
    private boolean isPlayerTurn(final Player shooter,
                                 final Player target,
                                 final String input) throws InterruptedException {
        if (input.equals("quit")) {
            shooter.setGameOver();
            target.setGameOver();
            LOG.info("Player " + shooter.getName() + " has ended the game!");
            return false;
        }
        try {
            shooter.shoot(input, target);
        } catch (IllegalCoordinatesException e) {
            LOG.trace(e.getMessage() + "\n");
            LOG.error("Player " + shooter.getName() + " has made wrong input!");
            Thread.sleep(TURN_DELAY);
            return true;
        }
        LOG.trace(shooter.getMessage() + "\n");
        LOG.info("Player " + shooter.getName() + " has made shot at " + input + " - " + shooter.getMessage());
        Thread.sleep(TURN_DELAY);
        return !shooter.getMessage().equals("MISSED!");
    }

    /**
     * Starts the bot turn to shot.
     *
     * @throws IOException          exception thrown by cls() method
     * @throws InterruptedException exception thrown by cls() method
     */
    private void turnOnBot() throws IOException, InterruptedException {
        if (player2.isGameOver()) {
            return;
        }
        do {
            cls();
            printMaps(player1);
            LOG.trace(player2.getName() + "'s shot: ");
            Thread.sleep(TURN_DELAY);
        } while (isPlayerTurn(player2, player1, "a1"));
    }

    /**
     * Starts the player's turn to shot.
     *
     * @param shooter player which is making a shot.
     * @param target  player against which shot is making.
     * @throws IOException          exception thrown by cls() method
     * @throws InterruptedException exception thrown by cls() method
     */
    private void turnOnHuman(final Player shooter,
                             final Player target)
            throws IOException, InterruptedException {
        if (shooter.isGameOver() || target.isGameOver()) {
            return;
        }
        String input;
        do {
            cls();
            printMaps(shooter);
            LOG.trace(shooter.getName() + "'s shot: ");
            input = SCANNER.nextLine().toLowerCase();
        } while (isPlayerTurn(shooter, target, input));
    }

    /**
     * Prints message about giving computer to another player
     * to allow it to make shot. And waits while another player press ENTER.
     *
     * @throws IOException          exception thrown by cls() method
     * @throws InterruptedException exception thrown by cls() method
     */
    private void showWaitScreen() throws InterruptedException, IOException {
        cls();
        LOG.trace(LOGO);
        LOG.trace("Give your computer to another player "
                + "and press ENTER...\n");
        SCANNER.nextLine();
    }

    /**
     * Provides all the game process.
     */
    public void show() {
        try {
            showMenu();
            LOG.info("------------------Game Started------------------");
            LOG.info("___________" + player1.getName() + " VS " + player2.getName() + "_____________");
            do {
                turnOnHuman(player1, player2);
                if (vsBot) {
                    turnOnBot();
                } else {
                    showWaitScreen();
                    turnOnHuman(player2, player1);
                    showWaitScreen();
                }
            } while (!player1.isGameOver() && !player2.isGameOver());
        } catch (IOException | InterruptedException e) {
            LOG.error(e.getMessage());
        }
    }
}
