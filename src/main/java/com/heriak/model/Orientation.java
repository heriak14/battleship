package com.heriak.model;

/**
 * Enum for storing orientation of ship.
 */
public enum Orientation {
    /**
     * Horizontal orientation.
     */
    HORIZONTAL,
    /**
     * Vertical orientation.
     */
    VERTICAL
}
