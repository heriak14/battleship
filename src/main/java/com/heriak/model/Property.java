package com.heriak.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * Class which provides simple properties getting from properties file.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public enum Property {

    INSTANCE;
    /**
     * Properties instance for containing a content from file.
     */
    private Properties properties;

    private final Logger LOG = LogManager.getLogger();

    Property() {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            LOG.trace("Couldn't load from properties file!");
            LOG.error(e.getMessage());
            System.exit(0);
        }
    }

    /**
     * Returns property from collection by its name.
     *
     * @param name name of property
     * @return property
     */
    public String getProperty(final String name) {
        return properties.getProperty(name);
    }


}
