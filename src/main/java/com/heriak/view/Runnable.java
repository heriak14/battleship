package com.heriak.view;

/**
 * Functional interface providing method for running different game-modes.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
@FunctionalInterface
public interface Runnable {
    /**
     * Runs one of two game-modes.
     */
    void run();
}
